package com.inter.interservice.controller;


import com.inter.commonutils.R;
import com.inter.interservice.entity.InterCategory;
import com.inter.interservice.service.InterCategoryService;
import io.swagger.annotations.ApiModelProperty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author mana
 * @since 2021-05-10
 */
@RestController
@RequestMapping("/interservice/category")
public class InterCategoryController {

    @Autowired
    private InterCategoryService categoryService;

    @PostMapping("addCate")
    @ApiModelProperty(value = "添加分类")
    public R addCate(@RequestBody InterCategory interCategory) {
        boolean flag = categoryService.save(interCategory);
        if (flag) {
            return R.ok();
        } else {
            return R.error();
        }
    }

    @GetMapping("list")
    @ApiModelProperty(value = "分类列表")
    public R list(){
        List list = categoryService.listCategory();
        return R.ok().data("list", list);
    }
}

