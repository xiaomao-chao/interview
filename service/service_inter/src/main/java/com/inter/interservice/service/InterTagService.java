package com.inter.interservice.service;

import com.inter.interservice.entity.InterTag;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author mana
 * @since 2021-05-10
 */
public interface InterTagService extends IService<InterTag> {

}
