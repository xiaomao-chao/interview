package com.inter.interservice.service.impl;

import com.inter.interservice.entity.InterStudyLog;
import com.inter.interservice.mapper.InterStudyLogMapper;
import com.inter.interservice.service.InterStudyLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mana
 * @since 2021-05-10
 */
@Service
public class InterStudyLogServiceImpl extends ServiceImpl<InterStudyLogMapper, InterStudyLog> implements InterStudyLogService {

}
