package com.inter.interservice.service;

import com.inter.interservice.entity.InterComment;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author mana
 * @since 2021-05-10
 */
public interface InterCommentService extends IService<InterComment> {

}
