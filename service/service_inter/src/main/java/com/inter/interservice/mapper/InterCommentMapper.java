package com.inter.interservice.mapper;

import com.inter.interservice.entity.InterComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author mana
 * @since 2021-05-10
 */
public interface InterCommentMapper extends BaseMapper<InterComment> {

}
