package com.inter.interservice.mapper;

import com.inter.interservice.entity.InterCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author mana
 * @since 2021-05-10
 */
public interface InterCategoryMapper extends BaseMapper<InterCategory> {

    void selectList();
}
