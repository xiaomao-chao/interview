package com.inter.interservice.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author mana
 * @since 2021-05-10
 */
@RestController
@RequestMapping("/interservice/inter-study-log")
public class InterStudyLogController {

}

