package com.inter.interservice.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author mana
 * @since 2021-05-10
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="InterItem对象", description="")
public class InterItem implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键id")
    @TableId(value = "id", type = IdType.AUTO)
    private String id;

    @ApiModelProperty(value = "题目标题")
    private String name;

    @ApiModelProperty(value = "题目描述")
    private String description;

    @ApiModelProperty(value = "标签id")
    private String tagId;

    @ApiModelProperty(value = "分类id")
    private String cateId;

    @ApiModelProperty(value = "题目内容")
    private String content;

    @ApiModelProperty(value = "访问数量")
    private Integer viewCount;

    @ApiModelProperty(value = "赞数量")
    private Integer praiseCount;

    @ApiModelProperty(value = "踩数量")
    private Integer disagreeCount;

    @ApiModelProperty(value = "是否删除 0-未删除  1-已删除")
    private Integer isDeleted;

    @ApiModelProperty(value = "创建时间")
    @TableField(fill = FieldFill.INSERT)
    private Date gmtCreated;

    @ApiModelProperty(value = "更新时间")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date gmtModified;


}
