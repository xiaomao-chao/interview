package com.inter.interservice.service.impl;

import com.inter.interservice.entity.InterTag;
import com.inter.interservice.mapper.InterTagMapper;
import com.inter.interservice.service.InterTagService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mana
 * @since 2021-05-10
 */
@Service
public class InterTagServiceImpl extends ServiceImpl<InterTagMapper, InterTag> implements InterTagService {

}
