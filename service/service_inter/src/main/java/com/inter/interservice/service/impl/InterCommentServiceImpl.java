package com.inter.interservice.service.impl;

import com.inter.interservice.entity.InterComment;
import com.inter.interservice.mapper.InterCommentMapper;
import com.inter.interservice.service.InterCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mana
 * @since 2021-05-10
 */
@Service
public class InterCommentServiceImpl extends ServiceImpl<InterCommentMapper, InterComment> implements InterCommentService {

}
