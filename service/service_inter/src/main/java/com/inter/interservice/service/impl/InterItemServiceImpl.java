package com.inter.interservice.service.impl;

import com.inter.interservice.entity.InterItem;
import com.inter.interservice.mapper.InterItemMapper;
import com.inter.interservice.service.InterItemService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author mana
 * @since 2021-05-10
 */
@Service
public class InterItemServiceImpl extends ServiceImpl<InterItemMapper, InterItem> implements InterItemService {

}
