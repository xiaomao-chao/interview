package com.inter.interservice;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@MapperScan("com.inter.interservice.mapper")
@ComponentScan("com.inter")
public class InterApplication {
    public static void main(String[] args) {
        SpringApplication.run(InterApplication.class, args);
    }
}
