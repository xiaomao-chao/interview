package com.inter.interservice.service;

import com.inter.interservice.entity.InterCategory;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author mana
 * @since 2021-05-10
 */
public interface InterCategoryService extends IService<InterCategory> {

    List listCategory();
}
